import * as io from 'io-ts'

const VoterValidator = io.type({
  id: io.string,
  username: io.string,
  vote: io.string,
});

export const PayloadValidator = io.type({
  id: io.string,
  username: io.string,
  showVotes: io.boolean,
  poked: io.boolean,
  voters: io.array(VoterValidator)
});

export type IState = io.TypeOf<typeof PayloadValidator>
export type IVoters = io.TypeOf<typeof VoterValidator>

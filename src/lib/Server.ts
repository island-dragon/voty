import { w3cwebsocket } from 'websocket'
import { toggleModal } from '../utils'
import { IState, PayloadValidator } from './validators'

class Server {
  public client!: w3cwebsocket
  public setVoters!: Function
  public setshowVotes!: Function
  public setPoked!: Function
  public setError!: Function

  public interval!: NodeJS.Timeout

  constructor() {
    this.connect()
  }

  connect = () => {
    this.client = new w3cwebsocket('wss://' + document.location.hostname + ':' + (process.env.REACT_APP_WS_PORT || 9000) + '/', 'echo-protocol')
    this.client.onerror = () => server.showError('Connection Error')
    this.client.onopen = () => {
      const username = document.cookie.replace('username=', '');

      if (username) {
        server.login(username, false)
      } else {
        toggleModal()
      }
    }

    this.client.onclose = () => server.showError('echo-protocol Client Closed')
    this.client.onmessage = (e) => {
      if (typeof e.data !== 'string') return

      try {
        const data = JSON.parse(e.data)
        if (PayloadValidator.is(data)) {
          server.pushData(data)
        }
      } catch (er) {
        console.log('Error parsing ' + e.data + ' ' + er)
      }
    }

    this.interval = setInterval(() => this.client.send(JSON.stringify({ event: 'ping' })) ,30000)
  }

  disconnect = () => {
    clearInterval(this.interval)
    this.client.onerror = () => {}
    this.client.onopen = () => {}
    this.client.onmessage = () => {}
    this.client.onclose = () => {}
    this.client.close()
  }

  registerHandlers = (setVoters: Function, setshowVotes: Function, setError: Function, setPoked: Function) => {
    this.setVoters = setVoters
    this.setshowVotes = setshowVotes
    this.setError = setError
    this.setPoked = setPoked
  }

  pushData = (data: IState) => {
    this.setPoked(data.poked)
    this.setVoters(data.voters)
    this.setshowVotes(data.showVotes)
  }

  showError = (msg: string) => {
    this.setError(msg)
  }

  logout = () => {
    document.cookie = 'username='
    this.disconnect()
    this.connect()
  }

  login = (username: string, toggle: boolean = true) => {
    if (toggle) toggleModal()

    document.cookie = 'username=' + username;
    this.client.send(JSON.stringify({ event: 'login', data: username }))
  }

  vote = (vote: string) => {
    this.client.send(JSON.stringify({ event: 'vote', data: vote }))
  }

  clearVotes = () => {
    this.client.send(JSON.stringify({ event: 'clear-votes' }))
  }

  showVotes = (value: boolean) => {
    this.client.send(JSON.stringify({ event: 'show-votes', data: value }))
  }

  pokeUser = (id: string) => {
    this.client.send(JSON.stringify({ event: 'poke', data: id }))
  }
}

export const server = new Server()

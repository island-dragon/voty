import React, { FunctionComponent } from 'react'

export const Summary: FunctionComponent<{ showVotes: boolean, summary: [string, number][] }> = ({ showVotes, summary }) => {
  const colors = [
    'voted'
  ]

  for (let i = 0; i < summary.length; i++) {
    if (i === 0) continue
    if (summary[i][1] !== summary[i-1][1]) {
      break;
    }

    if (summary[i][1] === summary[i-1][1]) {
      if (i === 1) {
        colors[0] = 'voted-tied'
      }
      colors.push('voted-tied')
    }


  }

  return <div className="col-6 text-center">
    <div className="row justify-content-center">
      <div className="col-3">
        <h4>Value</h4>
      </div>
      <div className="col-7">
        <h4># Votes</h4>
      </div>
    </div>
    {!showVotes && <div className="row justify-content-center">Voting in progress...</div>}
    {showVotes && summary.map((row, i) =>
      <div key={i} className={`row justify-content-center ${colors[i]}`}>
        <div className="col-3">
          {row[0] || 'Not Voted'}
        </div>
        <div className="col-7">
          {('✔️'.repeat(row[1]))}
        </div>
      </div>
    )}
  </div>
}

import React, { FunctionComponent } from 'react'
import { server } from '../lib/Server'

export const Modal: FunctionComponent<{ username: string, setUsername: Function }> = ({ username, setUsername }) =>
  <div className="modal" tabIndex={-1} role="dialog" id="login">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">Login</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          <form onSubmit={e => {
            e.preventDefault();
            (document.getElementById('name') as HTMLInputElement).value = ''
            server.login(username)
          }}>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                aria-describedby="nameHelp"
                placeholder="Enter name"
                onChange={e => setUsername(e.target.value)}
                autoFocus
              />
              <small id="nameHelp" className="form-text text-muted">This will be displayed to other users.</small>
            </div>
          </form>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-primary" onClick={e => {
            e.preventDefault();
            (document.getElementById('name') as HTMLInputElement).value = ''
            server.login(username)
          }}>Save changes</button>
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

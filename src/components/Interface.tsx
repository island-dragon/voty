import React from 'react'
import { server } from '../lib/Server'
import { Voting } from './Voting'
import { ShowVoter } from './ShowVoter'
import { Summary } from './Summary'
import { IVoters } from '../lib/validators'

interface IProps {
  voters: IVoters[]
  showVotes: boolean
  summary: [string, number][]
  myName: string
  logout: Function
  poke: Function
}

export const Interface = (props: IProps) =>
  <div>
    {props.myName ?
      <div className="float-right">
        {props.myName}
        <br />
        <a href="#logout" onClick={() => props.logout() && false}>Logout</a>
      </div>
      : <div></div>
    }

    <Voting />
    <div className="row mt-3"></div>
    <div className="row justify-content-center">
      <button className="btn btn-sm btn-primary" onClick={server.clearVotes}>Clear</button>
        &nbsp;
        <button className="btn btn-sm btn-success" onClick={() => server.showVotes(!props.showVotes)}>{props.showVotes ? 'Hide Votes' : 'Show Votes'}</button>
    </div>
    <div className="row mt-3"></div>
    <div className="row justify-content-center">
      <div className="col-6 text-center">
        <div className="row justify-content-center">
          <h4>Voters</h4>
        </div>
        {props.voters.map(voter =>
          <ShowVoter key={voter.id} poke={props.poke} uid={voter.id} name={voter.username} vote={voter.vote} showVotes={props.showVotes} myName={props.myName} />
        )}
      </div>
      <Summary showVotes={props.showVotes} summary={props.summary} />
    </div>
  </div>

import React, { FunctionComponent } from 'react'

export const Error: FunctionComponent<{ msg: string }> = ({ msg }) =>
  <div className="container">
    <div className="alert alert-danger" role="alert">
      {msg}
      <br />
      Refresh Page. This may be a momentary issue.
    </div>
  </div>

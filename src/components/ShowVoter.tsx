import React, { FunctionComponent } from 'react'

export const ShowVoter: FunctionComponent<{ uid: string, name: string, vote: string, showVotes: boolean, myName: string, poke: Function }> = ({ name, vote, showVotes, myName, uid, poke }) =>
  <div className="row justify-content-left voter">
    <div className={`col-10 text-left ${vote ? 'voted' : ''}`}>
      {vote ? '✔️' : <a href="#poke" onClick={() => poke(uid)}><span role="img" aria-label="Missing Vote">❔</span></a>} {name || 'Set Username'}
    </div>
    <div className="col-2 text-center">
      {showVotes || name === myName ? vote || 'Vote!' : '💭'}
    </div>
  </div>

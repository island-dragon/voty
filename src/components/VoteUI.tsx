import React, { FunctionComponent } from 'react'
import { server } from '../lib/Server'

export const VoteUI: FunctionComponent = ({ children }) =>
  <button type="button" className="btn btn-lg btn-secondary" onClick={e => server.vote(e.currentTarget.innerHTML)}>
    {children}
  </button>

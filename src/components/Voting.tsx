import React, { FunctionComponent } from 'react'
import { VoteUI } from './VoteUI'

export const Voting: FunctionComponent = () =>
  <div className="row justify-content-center">
    <div className="col-6 text-center">
      <div className="btn-group" role="group">
        {[1, 2, 4, 8, 10, 12, "?"].map(v =>
          <VoteUI key={v.toString()}>{v}</VoteUI>
        )}
      </div>
    </div>
  </div>

import React, { useState } from 'react'
import './App.scss'
import { server } from './lib/Server'
import { Modal } from './components/Modal'
import { Interface } from './components/Interface'
import { Error } from './components/Error'
import { IVoters } from './lib/validators'

let timeout: NodeJS.Timeout

export const App = () => {
  const initialVoters: IVoters[] = []
  const [voters, setVoters] = useState(initialVoters)
  const [username, setUsername] = useState(document.cookie.replace(/username=/, ''))
  const [showVotes, setshowVotes] = useState(false)
  const [poked, setPoked] = useState(false)
  const [error, setError] = useState('')

  const initialSummary: [string, number][] = []
  const summary = voters.reduce((sum, cur) => {
    const index = sum.findIndex(v => v[0] === cur.vote)
    if (index !== -1) {
      sum[index][1] += 1
      return sum
    }

    sum.push([cur.vote, 1])

    return sum
  }, initialSummary).sort((a,b) => b[1] - a[1])

  server.registerHandlers(setVoters, setshowVotes, setError, setPoked)

  let ui = null
  if (username) {
    ui = <Interface showVotes={showVotes} voters={voters} summary={summary} myName={username} poke={server.pokeUser} logout={() => { server.logout(); setUsername('') }} />
  }

  if (timeout) {
    clearTimeout(timeout)
  }
  timeout = setTimeout(() => setPoked(false), 1000)

  console.log(poked)
  return error
    ? <Error msg={error} />
    : <div className={'container ' + (poked === true ? 'poked' : '')}>
      {ui}
      <Modal username={username} setUsername={setUsername} />

    </div >
}

import $ from 'jquery'

export function toggleModal() {
  $('#login').modal('toggle')
  $('#name').focus()
}

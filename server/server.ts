import { server as WebSocketServer, connection } from 'websocket'
import * as http from 'https'
import * as crypto from 'crypto'
import * as fs from 'fs'

import { IState } from '../src/lib/validators'

const config = {
  PORT: process.env.PORT || 9000,
  HOST: process.env.HOST,
  SSL_KEY: process.env.SSL_KEY,
  SSL_PEM: process.env.SSL_PEM,
}

const state: IState = {
  id: '',
  username: '',
  showVotes: false,
  poked: false,
  voters: []
}

const options = {
  secure: true,
  key: fs.readFileSync(config.SSL_KEY),
  cert: fs.readFileSync(config.SSL_PEM)
}

const server = http.createServer(options, (request, response) => {
  console.log((new Date()) + ' Received request for ' + request.url)
  response.writeHead(404)
  response.end()
})

server.listen(config.PORT, () => {
  console.log(`${new Date()} Server is listening on port ${config.PORT}`)
})

const wsServer = new WebSocketServer({
  httpServer: server,
  autoAcceptConnections: false
})

const originIsAllowed = (origin) => {
  return new RegExp(`${config.HOST}`).test(origin)
}

const clients: { [key: string]: connection } = {}

wsServer.on('request', (request) => {
  if (!originIsAllowed(request.origin)) {
    // Make sure we only accept requests from an allowed origin
    request.reject()
    console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.')
    return
  }

  const con = request.accept('echo-protocol', request.origin)
  const id = crypto.randomBytes(20).toString('hex')

  clients[id] = con

  let username = ''

  console.log((new Date()) + ' Connection accepted with id ' + id)

  con.on('message', (message) => {
    if (message.type === 'utf8') {
      console.log('Received Message: ' + message.utf8Data)

      let msg = { event: 'none', data: null }
      try {
        msg = JSON.parse(message.utf8Data)
      } catch (e) {
        console.log('Error parsing ' + message.utf8Data + ' ' + e)
      }

      switch (msg.event) {
        case 'login':
          state.voters.push({ id, username: msg.data, vote: '' })
          username = msg.data
          break;
        case 'vote':
          const voter = state.voters.find(v => v.id === id)
          if (voter) {
            voter.vote = msg.data
          }
          break;
        case 'clear-votes':
          state.voters.forEach(v => v.vote = '')
          state.showVotes = false
          break;
        case 'show-votes':
          state.showVotes = msg.data
          break;
        case 'poke':
          if (clients[msg.data]) {
            clients[msg.data].send(JSON.stringify({ id, username, poked: true, ...state }))
            return
          }
      }

      wsServer.broadcastUTF(JSON.stringify({ id, username, poked: false, ...state }))
    }
  })
  con.on('close', (reasonCode, description) => {
    state.voters = state.voters.filter(v => v.id !== id)
    console.log((new Date()) + ' ' + id + ' disconnected.')
    wsServer.broadcastUTF(JSON.stringify({ id, username, ...state }))

    delete clients[id]
  })
})
